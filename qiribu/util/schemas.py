from qiribu import ma 
from marshmallow_sqlalchemy import ModelSchema
from qiribu.models import User, Transaction

class UserSchema(ModelSchema):
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'emp_ref', 'phone_number', 'nin', 'working_days', 'month', 'net_salary')

    
class TransactionSchema(ModelSchema):
    class Meta:
       fields = ('id', 'first_name', 'last_name', 'status', 'user_id', 'net_salary', 'month', 'request_amount', 'payable_amount', 'request_date', 'working_days')