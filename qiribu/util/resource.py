from calendar import monthrange
from datetime import datetime
import re
# calculate advance salary
# rules: employee is entitled to 75% of their daily earned salary
# formula: net_salary * 0.75 = advance_salary
# advance_salary/working_days = daily_advance_salary

def calculate_advance_salary(data):
    net_salary = data['net_salary']
    working_days = data['working_days']
    month = data['month']

    # get number of days in the month, year=current year
    days_in_month = monthrange(datetime.now().year, month)[1]

    daily_advance_salary = (net_salary * 0.75) / days_in_month
    advance_salary = daily_advance_salary * working_days
    return round(advance_salary, 2)

def verify_nin(nin):
    return re.match(r"^C[F|M]\d{2}[0-9]{5}[A-Z0-9]{5}$", nin)

def verify_emp_ref(emp_ref):
    return re.match(r"^REF\w{2}\d{3}$", emp_ref)


def verify_phone(phone):
    return re.match(r"^(077|078|076|070|075)\d{7}$", phone)

def verify_email(email):
    return re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", email)