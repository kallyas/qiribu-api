from qiribu.models import User, Transaction
from flask_jwt_extended import (
    jwt_required, get_jwt_claims
)
from flask_restful import Resource, reqparse
from qiribu.util.schemas import UserSchema

user_schema = UserSchema()


class UserResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    @jwt_required
    def get(self):
        claims = get_jwt_claims()
        user = User.find_by_id(claims['id'])

        if not user:
            return {'message': 'User not found'}, 404

        return user_schema.dump(user), 200
