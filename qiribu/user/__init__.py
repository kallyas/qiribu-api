from .resources import *

def user_routes(api):
    api.add_resource(UserResource, '/api/v1/user/me')