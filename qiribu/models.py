# create flask models
from datetime import datetime
from qiribu import db
from passlib.hash import pbkdf2_sha256
from sqlalchemy.ext.declarative import declared_attr

def create_employer_ref_id():
    # format: REF + 2 random letters + 3 random numbers
    import random
    import string
    ref_id = 'REF'
    for _ in range(2):
        ref_id += random.choice(string.ascii_letters)
    for _ in range(3):
        ref_id += random.choice(string.digits)
    return ref_id


class ExtraMixin(object):
    @declared_attr
    def created_by(cls):
        return db.Column(db.Integer, db.ForeignKey('user.id'))

    @declared_attr
    def updated_by(cls):
        return db.Column(db.Integer, db.ForeignKey('user.id'))

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

class Employer(db.Model):
    __tablename__ = 'employers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)
    bussiness_name = db.Column(db.String(255), nullable=False)
    bussiness_phone = db.Column(db.String(255), nullable=False)
    bussiness_address = db.Column(db.String(255), nullable=False)
    ref_id = db.Column(db.String(255), nullable=False, default=create_employer_ref_id, unique=True)

    users = db.relationship('User', backref='employer_user', lazy=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def update(cls):
        db.session.commit()

    @classmethod
    def find_by_phone(cls, phone_number):
        return cls.query.filter_by(phone_number=phone_number).first()



class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(80), nullable=False)
    last_name = db.Column(db.String(80), nullable=False)
    pin = db.Column(db.String(120), nullable=False)
    nin = db.Column(db.String(255), nullable=False, unique=True)
    phone_number = db.Column(db.String(120), nullable=False, unique=True)
    net_salary = db.Column(db.Integer, nullable=False, default=0)
    working_days = db.Column(db.Integer, nullable=False, default=0)
    month = db.Column(db.Integer, nullable=False, default=0)
    logged_in_number = db.Column(db.String(120), nullable=False, default=0)
    logged_in = db.Column(db.Boolean, default=False, )
    logged_in_date = db.Column(db.DateTime, default=datetime.utcnow)


    # emp_ref is a foreign key from Employer table
    emp_ref = db.Column(db.String(9), db.ForeignKey('employers.ref_id'), nullable=False)
    employer = db.relationship('Employer', backref='user_employer', lazy=True)
    
    def __repr__(self):
        return '<User {}>'.format(self.id)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def update(cls):
        db.session.commit()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_phone(cls, phone_number):
        return cls.query.filter_by(phone_number=phone_number).first()

    @classmethod
    def get_user_id(cls, phone_number):
        return cls.query.filter_by(phone_number=phone_number).first().id

    @classmethod
    def get_net_salary(cls, phone_number):
        return cls.query.filter_by(phone_number=phone_number)

    @staticmethod
    def generate_hash(pin):
        return pbkdf2_sha256.hash(pin)

    @staticmethod
    def verify_hash(pin, hash):
        return pbkdf2_sha256.verify(pin, hash)


class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    status = db.Column(db.String(120), nullable=False, default='pending')
    description = db.Column(db.String(120), nullable=False)
    request_amount = db.Column(db.Integer, nullable=False, default=0)
    payable_amount = db.Column(db.Integer, nullable=False, default=0)
    request_date = db.Column(db.DateTime, default=datetime.utcnow)

    user = db.relationship('User', backref='transaction', lazy=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def update(cls):
        db.session.commit()

    @classmethod
    def find_by_user_id(cls, user_id):
        return cls.query.filter_by(user_id=user_id).first()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_status(cls, status):
        return cls.query.filter_by(status=status).all()


class AdminUser(db.Model):
    __tablename__ = 'management'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def update(cls):
        db.session.commit()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    @staticmethod
    def generate_hash(password):
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return pbkdf2_sha256.verify(password, hash)


class RevokedToken(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)