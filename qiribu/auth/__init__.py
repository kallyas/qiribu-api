from .resources import *

def auth_routes(api):
    api.add_resource(UserLogin, "/api/v1/auth/login")
    api.add_resource(UserLogoutAccess, "/api/v1/auth/logout")
    api.add_resource(UserLogoutRefresh, "/api/v1/auth/logout/refresh")
    api.add_resource(UserRegister, "/api/v1/auth/register")
    api.add_resource(TokenRefresh, "/api/v1/auth/token/refresh")
    api.add_resource(AdminAuthResource, "/api/v1/auth/admin")
