from datetime import timedelta, datetime
import re

from qiribu.models import User, RevokedToken, AdminUser
from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    create_refresh_token,
    get_jwt_identity,
    get_raw_jwt,
    jwt_refresh_token_required,
    get_jwt_claims,
)
from flask_restful import Resource, reqparse
from qiribu.util.resource import verify_email, verify_emp_ref, verify_nin, verify_phone
from qiribu.util.schemas import UserSchema

user_schema = UserSchema()


class UserRegister(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('first_name', type=str, required=True, help='First Name is required')
        parser.add_argument('last_name', type=str, required=True, help='Last Name is required')
        parser.add_argument('phone_number', type=str, required=True, help='Phone Number is required')
        parser.add_argument('pin', type=str, required=True, help='Pin is required')
        parser.add_argument('nin', type=str, required=True, help='National ID is required')
        parser.add_argument('emp_ref', type=str, required=True, help='Employer Reference is required')

        data = parser.parse_args()

        if not verify_phone(data['phone_number']):
            return {"message": "Invalid phone number, use airtel or MTN"}, 400

        if not verify_emp_ref(data['emp_ref']):
            return {"message": "Invalid employer reference, use REFXXXXX"}, 400

        if not verify_nin(data['nin']):
            return {"message": "Invalid national id, use C[F|M]XXXXXXXXX"}, 400
            
        user = User.find_by_phone(data['phone_number'])

        if user:
            return {"message": "User already exists"}, 400

        user = User(
            first_name=data['first_name'],
            last_name=data['last_name'],
            phone_number=data['phone_number'],
            pin=User.generate_hash(data['pin']),
            nin=data['nin'],
            emp_ref=data['emp_ref'],
            month=datetime.now().month
        )

        user.save_to_db()

        return {"message": "User created successfully"}, 201


class UserLogin(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    def check_phone(self, phone):
        return re.match(r"^(077|078|076|070|075)\d{7}$", phone)

    def post(self):
        self.parser.add_argument(
            "phone_number", help="This is a required field", type=str, required=True
        )
        self.parser.add_argument(
            "pin", help="This is a required field", type=str, required=True
        )
        args = self.parser.parse_args()

        if not self.check_phone(args["phone_number"]):
            return {"message": "Invalid phone number, use airtel or MTN"}, 400

        user = User.find_by_phone(args["phone_number"])

        if not user:
            return {"message": "User not found"}, 404

        identity = {"id": user.id, "is_admin": False }

        if user and User.verify_hash(args["pin"], user.pin):
            access_token = create_access_token(
                identity=identity, expires_delta=timedelta(days=1)
            )
            refresh_token = create_refresh_token(identity=user.id)
            return {
                "access_token": access_token,
                "refresh_token": refresh_token,
                "user": {
                    "id": user.id,
                },
            }, 200
        return {"message": "Invalid pin"}, 400


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()["jti"]
        try:
            revoked_token = RevokedToken(jti=jti)
            revoked_token.add()
            return {"message": "Access token has been revoked"}, 200
        except:
            return {"message": "Something went wrong"}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()["jti"]
        try:
            revoked_token = RevokedToken(jti=jti)
            revoked_token.add()
            return {"message": "Refresh token has been revoked"}, 200
        except:
            return {"message": "Something went wrong"}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user, expires_delta=timedelta(days=1))
        refresh_token = create_refresh_token(identity=current_user)
        return {"access_token": access_token, "refresh_token": refresh_token}, 200



class AdminAuthResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    def post(self):
        self.parser.add_argument("email", type=str, help="Email is a required field", required=True)
        self.parser.add_argument("password", type=str, help="password is required", required=True)

        data = self.parser.parse_args()
        
        if not verify_email(data['email']):
            return {"message": "Invalid email"}, 400

        admin = AdminUser.find_by_email(data['email'])

        if not admin:
            return {"message": "Admin not found"}, 404

        identity = {
            "id": admin.id,
            "is_admin": True
        }

        if AdminUser.verify_hash(data['password'], admin.password):
            access_token = create_access_token(identity=identity, expires_delta=timedelta(days=1))
            refresh_token = create_refresh_token(identity=identity)
            return {
                "access_token": access_token,
                "refresh_token": refresh_token,
                "admin": {
                    "id": admin.id,
                },
            }, 200
        return {"message": "Invalid email or password"}, 400
