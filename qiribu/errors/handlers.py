from flask import Blueprint, jsonify, request

errors = Blueprint('errors', __name__)

@errors.app_errorhandler(404)
def not_found(error):
    return jsonify({'message': 'Resource at {} not found'.format(request.path)}), 404

@errors.app_errorhandler(403)
def forbidden(error):
    return jsonify({'message': 'Access Forbidden'}), 403


@errors.app_errorhandler(500)
def internal_server_error(error):
    return jsonify({'message': 'Internal Server Error'}), 500


@errors.app_errorhandler(405)
def method_not_allowed(error):
    return jsonify({'message': '{} Method Not Allowed on this resource'.format(request.method)}), 405

    