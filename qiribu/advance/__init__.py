from .resources import *

def transaction_routes(api):
    api.add_resource(TransactionResource,  "/api/v1/advance/check_balance")
    api.add_resource(AdvanceResource, "/api/v1/advance/request")