from qiribu.models import Transaction, User
from flask_restful import Resource, reqparse
from qiribu.util import calculate_advance_salary
from flask_jwt_extended import jwt_required, get_jwt_claims
from qiribu.util.schemas import TransactionSchema

transaction_schema = TransactionSchema()


class TransactionResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    @jwt_required
    def get(self):
        user_id = get_jwt_claims()["id"]
        transaction = User.query.join(Transaction).filter(User.id == user_id).first()

        if transaction:
            advance_salary = calculate_advance_salary(
                transaction_schema.dump(transaction)
            )
            return {"status": "success", "balance": advance_salary}
        return {"status": "fail", "message": "No transaction found"}


class AdvanceResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    @jwt_required
    def post(self):
        self.parser.add_argument(
            "request_amount",
            type=float,
            required=True,
            help="This field cannot be left blank!",
        )
        self.parser.add_argument(
            "description", type=str, required=True, help="This field cannot be blank"
        )

        data = self.parser.parse_args()

        if data["request_amount"] < 0:
            return {"status": "fail", "message": "Request amount cannot be negative"}, 400

        user = User.find_by_id(get_jwt_claims()["id"])

        # get transaction by current user where status is pending
        transaction = Transaction.query.filter_by(
            user_id=user.id, status="pending"
        ).first()

        if transaction:
            return {"status": "fail", "message": "You have a pending request"}, 400

        advance_salary = calculate_advance_salary(transaction_schema.dump(user))

        if advance_salary < data["request_amount"]:
            return {"status": "fail", "message": "Insufficient balance"}

        new_transaction = Transaction(
            user_id=user.id,
            description=data["description"],
            request_amount=data["request_amount"],
            payable_amount=data["request_amount"],
        )
        new_transaction.save_to_db()

        return {"status": "success", "message": "Request submitted successfully"}
