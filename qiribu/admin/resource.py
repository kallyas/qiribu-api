from qiribu.models import AdminUser, User, Transaction
from qiribu.util.schemas import UserSchema, TransactionSchema
from flask_restful import Resource, reqparse
from flask_jwt_extended import (
    jwt_required,
    get_jwt_identity,
    get_raw_jwt,
    get_jwt_claims,
)

from qiribu import db

user_schema = UserSchema()
users_schema = UserSchema(many=True)

transaction_schema = TransactionSchema()
transactions_schema = TransactionSchema(many=True)


class AdminResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    @jwt_required
    def put(self, user_id):
        if not get_jwt_claims()["is_admin"]:
            return {"message": "You are not authorized to perform this action"}, 401

        user_transaction = Transaction.find_by_user_id(user_id)
        if user_transaction:
            user_transaction.status = "approved"
            user_transaction.update()
            return {"message": "Transaction approved"}, 200
        else:
            return {"message": "Transaction not found"}, 404

    @jwt_required
    def get(self, user_id):
        if not get_jwt_claims()["is_admin"]:
            return {"message": "You are not authorized to perform this action"}, 401

        user_transaction = Transaction.find_by_user_id(user_id)
        if user_transaction:
            return transaction_schema.dump(user_transaction), 200
        else:
            return {"message": "Transaction not found"}, 404


class TransactionsResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    @jwt_required
    def get(self, trans_id=None, user_id=None):
        if not get_jwt_claims()["is_admin"]:
            return {"message": "You are not authorized to perform this action"}, 401

        if trans_id:
            transaction = Transaction.find_by_id(id=trans_id)
            if not transaction:
                return {"message": f"Transaction with id {trans_id} not found"}, 404
            return transaction_schema.dump(transaction)

        columns = Transaction.__table__.columns+[User.net_salary, User.month, User.working_days, User.first_name, User.last_name]
        transactions = db.session.query(*columns).join(User, User.id == Transaction.user_id).all()
        return transactions_schema.dump(transactions), 200