from .resource import *

def admin_routes(api):
    api.add_resource(AdminResource, '/api/v1/transactions/<int:user_id>/approve', '/api/v1/transactions/<int:user_id>')
    api.add_resource(TransactionsResource, '/api/v1/transactions', '/api/v1/transactions/<int:trans_id>')