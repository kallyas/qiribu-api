import os
from sqlalchemy import MetaData
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow
from flask_cors import CORS
from flask_migrate import Migrate
from .config import env_config
import logging
from dotenv import load_dotenv
load_dotenv()

convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=convention)

db = SQLAlchemy(metadata=metadata)
ma = Marshmallow()
migrate = Migrate()
api = Api()
jwt = JWTManager()
cors = CORS()

if os.getenv('FLASK_ENV') == 'development':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='logs/app.log')


def create_app(config_name=os.getenv('FLASK_ENV')):
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(env_config[config_name])
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    api.init_app(app)
    jwt.init_app(app)
    cors.init_app(app)

    from .errors.handlers import errors
    app.register_blueprint(errors)

    from qiribu.models import RevokedToken

    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decrypted_token):
        jti = decrypted_token['jti']
        return RevokedToken.is_jti_blacklisted(jti)

    
    @jwt.user_claims_loader
    def add_claims_to_access_token(identity):
        return identity

    @jwt.user_identity_loader
    def user_identity_lookup(user):
        return user

    return app


# API endpoints will be here
from qiribu.auth import auth_routes
from qiribu.advance import transaction_routes
from qiribu.user import user_routes
from qiribu.admin import admin_routes

auth_routes(api)
transaction_routes(api)
user_routes(api)
admin_routes(api)