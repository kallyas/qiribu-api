from tests.base_case import BaseCase
import json

user = {
    "first_name": "test",
    "last_name": "test",
    "phone_number": "0781234567",
    "pin": "1234",
    "emp_ref": "REFBJ375",
    "nin": "CF96005103HDNF"
}




class TestApi(BaseCase):
    def register_user(self):
        return self.app.post('/api/v1/auth/register', data=json.dumps(user), content_type='application/json')

    def test_register_user_success(self):
        response = self.app.post('/api/v1/auth/register', data=json.dumps(user), content_type='application/json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(json.loads(response.data)['message'], 'User created successfully')

    def test_login_success(self):
        self.register_user()
        response = self.app.post('/api/v1/auth/login', data=json.dumps({
            'phone_number': '0781234567',
            'pin': '1234'
        }), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('access_token', json.loads(response.data))
        self.assertIn('refresh_token', json.loads(response.data))

    def test_login_fail_wrong_pin(self):
        self.register_user()
        response = self.app.post('/api/v1/auth/login', data=json.dumps({
            'phone_number': '0781234567',
            'pin': '12345'
        }), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.data)['message'], 'Invalid pin')

    def test_login_fail_wrong_phone(self):
        self.register_user()
        response = self.app.post('/api/v1/auth/login', data=json.dumps({
            'phone_number': '07812345684',
            'pin': '1234'
        }), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.data)['message'], 'Invalid phone number, use airtel or MTN')

    def test_login_fail_no_user(self):
        response = self.app.post('/api/v1/auth/login', data=json.dumps({
            'phone_number': '0781234567',
            'pin': '1234'
        }), content_type='application/json')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(json.loads(response.data)['message'], 'User not found')

    def test_login_fail_no_phone_and_pin(self):
        response = self.app.post('/api/v1/auth/login', data=json.dumps({}), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.data)['message']['phone_number'], 'This is a required field')

    